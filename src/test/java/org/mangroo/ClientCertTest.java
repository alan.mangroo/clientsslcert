package org.mangroo;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClientCertTest {

    private static final String KEYSTOREPATH = "keystore.pfx"; // or .p12
    private static final String KEYSTOREPASS = "password";
    private static final String KEYPASS = "keypass";

    KeyStore readStore() throws Exception {
        FileInputStream fileInputStream =  new FileInputStream(KEYSTOREPATH);
        KeyStore keyStore = KeyStore.getInstance("PKCS12"); // or "PKCS12"
        keyStore.load(fileInputStream, KEYSTOREPASS.toCharArray());
        fileInputStream.close();
        Certificate badssl = keyStore.getCertificate("1");
        return keyStore;
    }

    @Test
    public void performClientRequest() throws Exception {

        KeyManagerFactory keyMgrFactory = KeyManagerFactory.getInstance("SunX509");
        keyMgrFactory.init(readStore(), "password".toCharArray());

        SSLContext sslCtx = SSLContext.getInstance("TLSv1.2");
        sslCtx.init(keyMgrFactory.getKeyManagers(), null, null);

        SSLContext sslContext = SSLContexts.custom()
                .loadKeyMaterial(readStore(),"password".toCharArray()) // use null as second param if you don't have a separate key password
                .build();

        SSLParameters sslParam = new SSLParameters();
        sslParam.setNeedClientAuth(true);

        HttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();
        HttpResponse response = httpClient.execute(new HttpGet("https://client.badssl.com/"));
        assertEquals(200, response.getStatusLine().getStatusCode());
        HttpEntity entity = response.getEntity();

        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());
        EntityUtils.consume(entity);
    }

    @Test
    public void plainReqeust() throws Throwable {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet request = new HttpGet("https://httpbin.org/get");

        CloseableHttpResponse response = httpClient.execute(request);

        System.out.println(response.getProtocolVersion());              // HTTP/1.1
        System.out.println(response.getStatusLine().getStatusCode());   // 200
        System.out.println(response.getStatusLine().getReasonPhrase()); // OK
        System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }
    }

    @Test
    public void serverCert() throws Throwable {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet request = new HttpGet("https://sha256.badssl.com/");

        // add request headers
        request.addHeader("custom-key", "mkyong");
        request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");
        CloseableHttpResponse response = httpClient.execute(request);

        System.out.println(response.getProtocolVersion());              // HTTP/1.1
        System.out.println(response.getStatusLine().getStatusCode());   // 200
        System.out.println(response.getStatusLine().getReasonPhrase()); // OK
        System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }
    }

    @Test
    public void clientCert() throws Throwable {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet request = new HttpGet("https://client.badssl.com/");

        // add request headers
        request.addHeader("custom-key", "mkyong");
        request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");
        CloseableHttpResponse response = httpClient.execute(request);

        System.out.println(response.getProtocolVersion());              // HTTP/1.1
        System.out.println(response.getStatusLine().getStatusCode());   // 200
        System.out.println(response.getStatusLine().getReasonPhrase()); // OK
        System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

        HttpEntity entity = response.getEntity();
        if (entity != null) {
            // return it as a String
            String result = EntityUtils.toString(entity);
            System.out.println(result);
        }
    }
}
